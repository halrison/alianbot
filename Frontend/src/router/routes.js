import MainLayout from '../layouts/MainLayout.vue'
import IndexPage from '../pages/IndexPage.vue'
import ErrorNotFound from '../layouts/ErrorNotFound.vue'
import StartUse from '../layouts/StartUse.vue'
const routes = [
  {
    path: '/admin',
    component:MainLayout,
    children: [
      {
        path: 'dashboard',
        component: () => import('../pages/Dashboard.vue')
      },
      {
        path: 'command',
        component:()=>import('../pages/Command.vue')
      },
      {
        path: 'song',
        component:()=>import('../pages/Song.vue')
      },
      {
        path: 'vote',
        component:()=>import('../pages/VoteSummary.vue')
      },
      {
        path: 'vote?id=:id',
        component:()=>import('../pages/VoteDetail.vue')
      }
    ]
  },
  {
    path: '/',
    component: IndexPage
  },
  {
    path: '/start-use',
    component:StartUse
  },
  {
    path: '/:catchAll(.*)*',
    component: ErrorNotFound
  }
]

export default routes
